UltraEdit ist ein Shareware Text-Editor, den man zum komfortablen editieren von ASCII-Dateien,
wie z.B. den GOTHIC-Scripten benutzen kann.
Für Syntax-Highlighting bei den GOTHIC-Scripten muß einfach die Datei WORDFILE.TXT
im Installationsverzeichnis von UltraEdit durch die Version aus diesem Verzeichnis ersetzt werden.

Weitere Informationen zu UltraEdit gibt es unter folgender Adresse:
http://www.ultraedit.com